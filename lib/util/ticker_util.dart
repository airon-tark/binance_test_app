import 'package:test_binance/model/ticker.dart';

/// 24H difference between [open] and [close] int percentage
double? lastDayDiff(BinanceTicker ticker) {
  final o = double.tryParse(ticker.open);
  final c = double.tryParse(ticker.close);

  if (o == null || c == null) {
    return null;
  }
  return (c - o) / c * 100;
}