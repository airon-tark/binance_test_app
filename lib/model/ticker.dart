class BinanceTicker {
  late String name;
  late String close;
  late String open;
  late String quantity;

  BinanceTicker({
    required this.name,
    required this.close,
    required this.open,
    required this.quantity,
  });

  BinanceTicker.fromJson(dynamic json) {
    this.name = json["s"];
    this.close = json["c"];
    this.open = json["o"];
    this.quantity = json["q"];
  }
}
