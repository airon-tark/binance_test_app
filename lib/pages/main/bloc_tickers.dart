import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_binance/model/ticker.dart';
import 'package:test_binance/services/binance_service.dart';

const maxCount = 100;

class TickerCubit extends Cubit<List<BinanceTicker>> {
  late final StreamSubscription<List<BinanceTicker>> _tickerStream;

  /// I decided to store 100 tickers and refresh them as they arrive
  Map<String, BinanceTicker> _tickers = {};

  late final BinanceService service;

  TickerCubit({required this.service}) : super([]) {
    _tickerStream = service.eventsStream.listen((event) {
      event.forEach((element) {
        // add any new items up to 100 or update existing otherwise
        if (_tickers.length < maxCount || _tickers.containsKey(element.name)) {
          _tickers[element.name] = element;
        }
      });

      // event.sort((t1, t2) => t1.name.compareTo(t2.name));
      /// decided not to sort, since it simply slows application
      /// and [toList] function already returns tickers in some order
      /// not sure what order... but it's good enough

      emit(_tickers.values.toList());
    });
  }

  @override
  Future<void> close() {
    _tickerStream.cancel();
    return super.close();
  }
}
