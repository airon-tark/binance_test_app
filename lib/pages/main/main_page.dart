import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_binance/model/ticker.dart';
import 'package:test_binance/pages/main/bloc_tickers.dart';
import 'package:test_binance/services/binance_service.dart';
import 'package:test_binance/widgets/ticker_list_widget.dart';

class MainPage extends StatefulWidget {
  @override
  createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  var _search = '';

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TickerCubit>(
      create: (c) => TickerCubit(
        service: c.read<BinanceService>(),
      ),
      child: BlocBuilder<TickerCubit, List<BinanceTicker>>(
        builder: (c, s) {
          return Scaffold(
            appBar: AppBar(
              title: _searchField(),
            ),
            body: _tickersList(s),
          );
        },
      ),
    );
  }

  Widget _tickersList(List<BinanceTicker> state) {
    final tickers = _search.isEmpty
        ? state
        : state
            .where((t) => t.name.toLowerCase().contains(_search.toLowerCase()))
            .toList();

    return ListView.builder(
      itemCount: tickers.length,
      padding: EdgeInsets.only(
        left: 12,
        right: 12,
      ),
      itemBuilder: (c, index) => Container(
        padding: EdgeInsets.only(
          top: 12,
        ),
        child: TickerListWidget(
          ticker: tickers[index],
        ),
      ),
    );
  }

  Widget _searchField() => TextField(
        maxLines: 1,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          isDense: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          prefixIcon: Icon(
            Icons.search,
            size: 24,
          ),
          hintText: 'Search',
        ),
        onChanged: (text) {
          setState(() => _search = text);
        },
      );
}
