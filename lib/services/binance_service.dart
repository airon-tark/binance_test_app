import 'dart:async';
import 'dart:convert';

import 'package:test_binance/model/ticker.dart';
import 'package:web_socket_channel/io.dart';

/// service is something that manages data from repositories
/// requests, stores and etc.
/// services more bound to data stored on server or local storage
/// while BLOCs more bound to UI
///
/// such decision allows multiple BLOCs to subscribe for single service which handles all information
class BinanceService {

  static const endPoint = 'wss://stream.binance.com:9443/ws/!miniTicker@arr';

  final _eventController = StreamController<List<BinanceTicker>>.broadcast();

  Stream<List<BinanceTicker>> get eventsStream => _eventController.stream;

  IOWebSocketChannel? _channel;

  void start() {
    _channel = IOWebSocketChannel.connect(endPoint);

    _channel!.stream.listen((data) {
      /// Parsing JSON
      _eventController.add((jsonDecode(data) as List)
          .map((m) => BinanceTicker.fromJson(m))
          .toList());
    });
  }

  void stop() {
    _eventController.close();
  }
}
