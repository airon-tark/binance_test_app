import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_binance/pages/main/main_page.dart';
import 'package:test_binance/services/binance_service.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /// services is architectural solution that I normally use in application
    /// usually they started when user logs in
    /// and stopped when logs out
    ///
    /// or something like that
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        accentColor: Colors.white38,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        brightness: Brightness.dark,
      ),
      home: Provider<BinanceService>(
        create: (c) => BinanceService()..start(),
        dispose: (c, service) => service.stop(),
        child: MainPage(),
      ),
    );
  }
}
